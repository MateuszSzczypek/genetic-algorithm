﻿namespace GeneticAlgorithm
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.aTextBox = new System.Windows.Forms.TextBox();
            this.bTextBox = new System.Windows.Forms.TextBox();
            this.dTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.xrealDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fxDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gxDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pxDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qxDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.xRealAfterSelectionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.xToCrossingDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.xToCrossingBinDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pcDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.posperityXDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bitesToChangeIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.xBinAfterMutationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.xAfterMutationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fxFinalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.geneticAlgorithmDataGridV2BindingSource7 = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.nTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tTextBox = new System.Windows.Forms.TextBox();
            this.dataGroupBox = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.filePathTextBox = new System.Windows.Forms.TextBox();
            this.TestingButton = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.isEliteCheckBox = new System.Windows.Forms.CheckBox();
            this.pmTextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.pkTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.ErrorLabel = new System.Windows.Forms.Label();
            this.geneticAlgorithmDataGridV2BindingSource6 = new System.Windows.Forms.BindingSource(this.components);
            this.geneticAlgorithmDataGridV2BindingSource5 = new System.Windows.Forms.BindingSource(this.components);
            this.geneticAlgorithmDataGridV2BindingSource4 = new System.Windows.Forms.BindingSource(this.components);
            this.geneticAlgorithmDataGridV2BindingSource3 = new System.Windows.Forms.BindingSource(this.components);
            this.geneticAlgorithmDataGridV2BindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.geneticAlgorithmDataGridV2BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.geneticAlgorithmDataGridBindingSource5 = new System.Windows.Forms.BindingSource(this.components);
            this.geneticAlgorithmDataGridBindingSource4 = new System.Windows.Forms.BindingSource(this.components);
            this.geneticAlgorithmDataGridBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.geneticAlgorithmDataGridBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.geneticAlgorithmDataGridBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.geneticAlgorithmDataGridBindingSource3 = new System.Windows.Forms.BindingSource(this.components);
            this.geneticAlgorithmDataGridV2BindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.geneticAlgorithmDataGridV2BindingSource7)).BeginInit();
            this.dataGroupBox.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.geneticAlgorithmDataGridV2BindingSource6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.geneticAlgorithmDataGridV2BindingSource5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.geneticAlgorithmDataGridV2BindingSource4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.geneticAlgorithmDataGridV2BindingSource3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.geneticAlgorithmDataGridV2BindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.geneticAlgorithmDataGridV2BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.geneticAlgorithmDataGridBindingSource5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.geneticAlgorithmDataGridBindingSource4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.geneticAlgorithmDataGridBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.geneticAlgorithmDataGridBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.geneticAlgorithmDataGridBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.geneticAlgorithmDataGridBindingSource3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.geneticAlgorithmDataGridV2BindingSource2)).BeginInit();
            this.SuspendLayout();
            // 
            // aTextBox
            // 
            this.aTextBox.Location = new System.Drawing.Point(100, 19);
            this.aTextBox.Name = "aTextBox";
            this.aTextBox.Size = new System.Drawing.Size(37, 20);
            this.aTextBox.TabIndex = 1;
            this.aTextBox.Text = "-4";
            // 
            // bTextBox
            // 
            this.bTextBox.Location = new System.Drawing.Point(180, 19);
            this.bTextBox.Name = "bTextBox";
            this.bTextBox.Size = new System.Drawing.Size(36, 20);
            this.bTextBox.TabIndex = 2;
            this.bTextBox.Text = "12";
            // 
            // dTextBox
            // 
            this.dTextBox.Location = new System.Drawing.Point(126, 56);
            this.dTextBox.Name = "dTextBox";
            this.dTextBox.Size = new System.Drawing.Size(74, 20);
            this.dTextBox.TabIndex = 3;
            this.dTextBox.Text = "0,001";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "X0 e <a, b>";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(66, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = ", a =";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(143, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = ", b = ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(29, 59);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(91, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = ", dokładność d = ";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(921, 19);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(165, 59);
            this.button1.TabIndex = 10;
            this.button1.Text = "Rozpocznij działanie algorytmu";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.xrealDataGridViewTextBoxColumn,
            this.fxDataGridViewTextBoxColumn,
            this.gxDataGridViewTextBoxColumn,
            this.pxDataGridViewTextBoxColumn,
            this.qxDataGridViewTextBoxColumn,
            this.rDataGridViewTextBoxColumn,
            this.xRealAfterSelectionDataGridViewTextBoxColumn,
            this.xToCrossingDataGridViewTextBoxColumn,
            this.xToCrossingBinDataGridViewTextBoxColumn,
            this.pcDataGridViewTextBoxColumn,
            this.posperityXDataGridViewTextBoxColumn,
            this.bitesToChangeIDDataGridViewTextBoxColumn,
            this.xBinAfterMutationDataGridViewTextBoxColumn,
            this.xAfterMutationDataGridViewTextBoxColumn,
            this.fxFinalDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.geneticAlgorithmDataGridV2BindingSource7;
            this.dataGridView1.Location = new System.Drawing.Point(13, 130);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1647, 317);
            this.dataGridView1.TabIndex = 11;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            // 
            // xrealDataGridViewTextBoxColumn
            // 
            this.xrealDataGridViewTextBoxColumn.DataPropertyName = "Xreal";
            this.xrealDataGridViewTextBoxColumn.HeaderText = "Xreal";
            this.xrealDataGridViewTextBoxColumn.Name = "xrealDataGridViewTextBoxColumn";
            // 
            // fxDataGridViewTextBoxColumn
            // 
            this.fxDataGridViewTextBoxColumn.DataPropertyName = "Fx";
            this.fxDataGridViewTextBoxColumn.HeaderText = "Fx";
            this.fxDataGridViewTextBoxColumn.Name = "fxDataGridViewTextBoxColumn";
            // 
            // gxDataGridViewTextBoxColumn
            // 
            this.gxDataGridViewTextBoxColumn.DataPropertyName = "Gx";
            this.gxDataGridViewTextBoxColumn.HeaderText = "Gx";
            this.gxDataGridViewTextBoxColumn.Name = "gxDataGridViewTextBoxColumn";
            // 
            // pxDataGridViewTextBoxColumn
            // 
            this.pxDataGridViewTextBoxColumn.DataPropertyName = "Px";
            this.pxDataGridViewTextBoxColumn.HeaderText = "Px";
            this.pxDataGridViewTextBoxColumn.Name = "pxDataGridViewTextBoxColumn";
            // 
            // qxDataGridViewTextBoxColumn
            // 
            this.qxDataGridViewTextBoxColumn.DataPropertyName = "Qx";
            this.qxDataGridViewTextBoxColumn.HeaderText = "Qx";
            this.qxDataGridViewTextBoxColumn.Name = "qxDataGridViewTextBoxColumn";
            // 
            // rDataGridViewTextBoxColumn
            // 
            this.rDataGridViewTextBoxColumn.DataPropertyName = "R";
            this.rDataGridViewTextBoxColumn.HeaderText = "R";
            this.rDataGridViewTextBoxColumn.Name = "rDataGridViewTextBoxColumn";
            // 
            // xRealAfterSelectionDataGridViewTextBoxColumn
            // 
            this.xRealAfterSelectionDataGridViewTextBoxColumn.DataPropertyName = "X_RealAfterSelection";
            this.xRealAfterSelectionDataGridViewTextBoxColumn.HeaderText = "X_RealAfterSelection";
            this.xRealAfterSelectionDataGridViewTextBoxColumn.Name = "xRealAfterSelectionDataGridViewTextBoxColumn";
            // 
            // xToCrossingDataGridViewTextBoxColumn
            // 
            this.xToCrossingDataGridViewTextBoxColumn.DataPropertyName = "X_ToCrossing";
            this.xToCrossingDataGridViewTextBoxColumn.HeaderText = "X_ToCrossing";
            this.xToCrossingDataGridViewTextBoxColumn.Name = "xToCrossingDataGridViewTextBoxColumn";
            // 
            // xToCrossingBinDataGridViewTextBoxColumn
            // 
            this.xToCrossingBinDataGridViewTextBoxColumn.DataPropertyName = "X_ToCrossingBin";
            this.xToCrossingBinDataGridViewTextBoxColumn.HeaderText = "X_ToCrossingBin";
            this.xToCrossingBinDataGridViewTextBoxColumn.Name = "xToCrossingBinDataGridViewTextBoxColumn";
            // 
            // pcDataGridViewTextBoxColumn
            // 
            this.pcDataGridViewTextBoxColumn.DataPropertyName = "Pc";
            this.pcDataGridViewTextBoxColumn.HeaderText = "Pc";
            this.pcDataGridViewTextBoxColumn.Name = "pcDataGridViewTextBoxColumn";
            // 
            // posperityXDataGridViewTextBoxColumn
            // 
            this.posperityXDataGridViewTextBoxColumn.DataPropertyName = "PosperityX";
            this.posperityXDataGridViewTextBoxColumn.HeaderText = "PosperityX";
            this.posperityXDataGridViewTextBoxColumn.Name = "posperityXDataGridViewTextBoxColumn";
            // 
            // bitesToChangeIDDataGridViewTextBoxColumn
            // 
            this.bitesToChangeIDDataGridViewTextBoxColumn.DataPropertyName = "BitesToChangeID";
            this.bitesToChangeIDDataGridViewTextBoxColumn.HeaderText = "BitesToChangeID";
            this.bitesToChangeIDDataGridViewTextBoxColumn.Name = "bitesToChangeIDDataGridViewTextBoxColumn";
            // 
            // xBinAfterMutationDataGridViewTextBoxColumn
            // 
            this.xBinAfterMutationDataGridViewTextBoxColumn.DataPropertyName = "XBinAfterMutation";
            this.xBinAfterMutationDataGridViewTextBoxColumn.HeaderText = "XBinAfterMutation";
            this.xBinAfterMutationDataGridViewTextBoxColumn.Name = "xBinAfterMutationDataGridViewTextBoxColumn";
            // 
            // xAfterMutationDataGridViewTextBoxColumn
            // 
            this.xAfterMutationDataGridViewTextBoxColumn.DataPropertyName = "X_AfterMutation";
            this.xAfterMutationDataGridViewTextBoxColumn.HeaderText = "X_AfterMutation";
            this.xAfterMutationDataGridViewTextBoxColumn.Name = "xAfterMutationDataGridViewTextBoxColumn";
            // 
            // fxFinalDataGridViewTextBoxColumn
            // 
            this.fxFinalDataGridViewTextBoxColumn.DataPropertyName = "FxFinal";
            this.fxFinalDataGridViewTextBoxColumn.HeaderText = "FxFinal";
            this.fxFinalDataGridViewTextBoxColumn.Name = "fxFinalDataGridViewTextBoxColumn";
            // 
            // geneticAlgorithmDataGridV2BindingSource7
            // 
            this.geneticAlgorithmDataGridV2BindingSource7.DataSource = typeof(GeneticAlgorithm.Models.GeneticAlgorithmDataGridV2);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(239, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = ", liczba osobników; ";
            // 
            // nTextBox
            // 
            this.nTextBox.Location = new System.Drawing.Point(345, 22);
            this.nTextBox.Name = "nTextBox";
            this.nTextBox.Size = new System.Drawing.Size(47, 20);
            this.nTextBox.TabIndex = 13;
            this.nTextBox.Text = "30";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(255, 59);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = ", liczba pokoleń:";
            // 
            // tTextBox
            // 
            this.tTextBox.Location = new System.Drawing.Point(345, 56);
            this.tTextBox.Name = "tTextBox";
            this.tTextBox.Size = new System.Drawing.Size(47, 20);
            this.tTextBox.TabIndex = 15;
            this.tTextBox.Text = "110";
            // 
            // dataGroupBox
            // 
            this.dataGroupBox.Controls.Add(this.groupBox1);
            this.dataGroupBox.Controls.Add(this.isEliteCheckBox);
            this.dataGroupBox.Controls.Add(this.pmTextBox);
            this.dataGroupBox.Controls.Add(this.label8);
            this.dataGroupBox.Controls.Add(this.label6);
            this.dataGroupBox.Controls.Add(this.tTextBox);
            this.dataGroupBox.Controls.Add(this.pkTextBox);
            this.dataGroupBox.Controls.Add(this.label7);
            this.dataGroupBox.Controls.Add(this.label2);
            this.dataGroupBox.Controls.Add(this.button1);
            this.dataGroupBox.Controls.Add(this.aTextBox);
            this.dataGroupBox.Controls.Add(this.nTextBox);
            this.dataGroupBox.Controls.Add(this.bTextBox);
            this.dataGroupBox.Controls.Add(this.label1);
            this.dataGroupBox.Controls.Add(this.dTextBox);
            this.dataGroupBox.Controls.Add(this.label3);
            this.dataGroupBox.Controls.Add(this.label4);
            this.dataGroupBox.Controls.Add(this.label5);
            this.dataGroupBox.Location = new System.Drawing.Point(12, 36);
            this.dataGroupBox.Name = "dataGroupBox";
            this.dataGroupBox.Size = new System.Drawing.Size(1097, 88);
            this.dataGroupBox.TabIndex = 16;
            this.dataGroupBox.TabStop = false;
            this.dataGroupBox.Text = "Data input";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.filePathTextBox);
            this.groupBox1.Controls.Add(this.TestingButton);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Location = new System.Drawing.Point(636, 16);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(279, 66);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Testing";
            // 
            // filePathTextBox
            // 
            this.filePathTextBox.Location = new System.Drawing.Point(138, 40);
            this.filePathTextBox.Name = "filePathTextBox";
            this.filePathTextBox.Size = new System.Drawing.Size(135, 20);
            this.filePathTextBox.TabIndex = 23;
            this.filePathTextBox.Text = "C:\\Users\\Pul\\Desktop\\";
            // 
            // TestingButton
            // 
            this.TestingButton.Location = new System.Drawing.Point(6, 12);
            this.TestingButton.Name = "TestingButton";
            this.TestingButton.Size = new System.Drawing.Size(267, 23);
            this.TestingButton.TabIndex = 21;
            this.TestingButton.Text = "Testuj";
            this.TestingButton.UseVisualStyleBackColor = true;
            this.TestingButton.Click += new System.EventHandler(this.TestingButton_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 43);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(126, 13);
            this.label9.TabIndex = 22;
            this.label9.Text = "Ścieżka zapisu pliku csv:";
            // 
            // isEliteCheckBox
            // 
            this.isEliteCheckBox.AutoSize = true;
            this.isEliteCheckBox.Checked = true;
            this.isEliteCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.isEliteCheckBox.Location = new System.Drawing.Point(206, 59);
            this.isEliteCheckBox.Name = "isEliteCheckBox";
            this.isEliteCheckBox.Size = new System.Drawing.Size(46, 17);
            this.isEliteCheckBox.TabIndex = 20;
            this.isEliteCheckBox.Text = "Elita";
            this.isEliteCheckBox.UseVisualStyleBackColor = true;
            // 
            // pmTextBox
            // 
            this.pmTextBox.Location = new System.Drawing.Point(583, 56);
            this.pmTextBox.Name = "pmTextBox";
            this.pmTextBox.Size = new System.Drawing.Size(47, 20);
            this.pmTextBox.TabIndex = 19;
            this.pmTextBox.Text = "0,0005";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(421, 59);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(156, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = ", prawdopodobieństwo mutacji; ";
            // 
            // pkTextBox
            // 
            this.pkTextBox.Location = new System.Drawing.Point(599, 24);
            this.pkTextBox.Name = "pkTextBox";
            this.pkTextBox.Size = new System.Drawing.Size(31, 20);
            this.pkTextBox.TabIndex = 17;
            this.pkTextBox.Text = "0,85";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(421, 28);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(181, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = ", prawdopodobieństwo krzyżowania; ";
            // 
            // ErrorLabel
            // 
            this.ErrorLabel.AutoSize = true;
            this.ErrorLabel.ForeColor = System.Drawing.Color.Red;
            this.ErrorLabel.Location = new System.Drawing.Point(112, 17);
            this.ErrorLabel.Name = "ErrorLabel";
            this.ErrorLabel.Size = new System.Drawing.Size(0, 13);
            this.ErrorLabel.TabIndex = 17;
            // 
            // geneticAlgorithmDataGridV2BindingSource6
            // 
            this.geneticAlgorithmDataGridV2BindingSource6.DataSource = typeof(GeneticAlgorithm.Models.GeneticAlgorithmDataGridV2);
            // 
            // geneticAlgorithmDataGridV2BindingSource5
            // 
            this.geneticAlgorithmDataGridV2BindingSource5.DataSource = typeof(GeneticAlgorithm.Models.GeneticAlgorithmDataGridV2);
            // 
            // geneticAlgorithmDataGridV2BindingSource4
            // 
            this.geneticAlgorithmDataGridV2BindingSource4.DataSource = typeof(GeneticAlgorithm.Models.GeneticAlgorithmDataGridV2);
            // 
            // geneticAlgorithmDataGridV2BindingSource3
            // 
            this.geneticAlgorithmDataGridV2BindingSource3.DataSource = typeof(GeneticAlgorithm.Models.GeneticAlgorithmDataGridV2);
            // 
            // geneticAlgorithmDataGridV2BindingSource1
            // 
            this.geneticAlgorithmDataGridV2BindingSource1.DataSource = typeof(GeneticAlgorithm.Models.GeneticAlgorithmDataGridV2);
            // 
            // geneticAlgorithmDataGridV2BindingSource
            // 
            this.geneticAlgorithmDataGridV2BindingSource.DataSource = typeof(GeneticAlgorithm.Models.GeneticAlgorithmDataGridV2);
            // 
            // geneticAlgorithmDataGridBindingSource5
            // 
            this.geneticAlgorithmDataGridBindingSource5.DataSource = typeof(GeneticAlgorithm.Models.GeneticAlgorithmDataGrid);
            // 
            // geneticAlgorithmDataGridBindingSource4
            // 
            this.geneticAlgorithmDataGridBindingSource4.DataSource = typeof(GeneticAlgorithm.Models.GeneticAlgorithmDataGrid);
            // 
            // geneticAlgorithmDataGridBindingSource2
            // 
            this.geneticAlgorithmDataGridBindingSource2.DataSource = typeof(GeneticAlgorithm.Models.GeneticAlgorithmDataGrid);
            // 
            // geneticAlgorithmDataGridBindingSource
            // 
            this.geneticAlgorithmDataGridBindingSource.DataSource = typeof(GeneticAlgorithm.Models.GeneticAlgorithmDataGrid);
            // 
            // geneticAlgorithmDataGridBindingSource1
            // 
            this.geneticAlgorithmDataGridBindingSource1.DataSource = typeof(GeneticAlgorithm.Models.GeneticAlgorithmDataGrid);
            // 
            // geneticAlgorithmDataGridBindingSource3
            // 
            this.geneticAlgorithmDataGridBindingSource3.DataSource = typeof(GeneticAlgorithm.Models.GeneticAlgorithmDataGrid);
            // 
            // geneticAlgorithmDataGridV2BindingSource2
            // 
            this.geneticAlgorithmDataGridV2BindingSource2.DataSource = typeof(GeneticAlgorithm.Models.GeneticAlgorithmDataGridV2);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1692, 466);
            this.Controls.Add(this.ErrorLabel);
            this.Controls.Add(this.dataGroupBox);
            this.Controls.Add(this.dataGridView1);
            this.Name = "MainForm";
            this.Text = "Algorytm genetyczny";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.geneticAlgorithmDataGridV2BindingSource7)).EndInit();
            this.dataGroupBox.ResumeLayout(false);
            this.dataGroupBox.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.geneticAlgorithmDataGridV2BindingSource6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.geneticAlgorithmDataGridV2BindingSource5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.geneticAlgorithmDataGridV2BindingSource4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.geneticAlgorithmDataGridV2BindingSource3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.geneticAlgorithmDataGridV2BindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.geneticAlgorithmDataGridV2BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.geneticAlgorithmDataGridBindingSource5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.geneticAlgorithmDataGridBindingSource4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.geneticAlgorithmDataGridBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.geneticAlgorithmDataGridBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.geneticAlgorithmDataGridBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.geneticAlgorithmDataGridBindingSource3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.geneticAlgorithmDataGridV2BindingSource2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox aTextBox;
        private System.Windows.Forms.TextBox bTextBox;
        private System.Windows.Forms.TextBox dTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource geneticAlgorithmDataGridBindingSource;
        private System.Windows.Forms.BindingSource geneticAlgorithmDataGridBindingSource2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox nTextBox;
        private System.Windows.Forms.BindingSource geneticAlgorithmDataGridBindingSource1;
        private System.Windows.Forms.BindingSource geneticAlgorithmDataGridBindingSource3;
        private System.Windows.Forms.DataGridViewTextBoxColumn xinDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn xramDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource geneticAlgorithmDataGridBindingSource4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tTextBox;
        private System.Windows.Forms.BindingSource geneticAlgorithmDataGridBindingSource5;
        private System.Windows.Forms.GroupBox dataGroupBox;
        private System.Windows.Forms.TextBox pmTextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox pkTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.BindingSource geneticAlgorithmDataGridV2BindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn xBinAfterSelectionDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource geneticAlgorithmDataGridV2BindingSource1;
        private System.Windows.Forms.BindingSource geneticAlgorithmDataGridV2BindingSource2;
        private System.Windows.Forms.DataGridViewTextBoxColumn biteToChangeIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource geneticAlgorithmDataGridV2BindingSource3;
        private System.Windows.Forms.BindingSource geneticAlgorithmDataGridV2BindingSource4;
        private System.Windows.Forms.BindingSource geneticAlgorithmDataGridV2BindingSource5;
        private System.Windows.Forms.BindingSource geneticAlgorithmDataGridV2BindingSource6;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn xrealDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fxDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn gxDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pxDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn qxDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn xRealAfterSelectionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn xToCrossingDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn xToCrossingBinDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pcDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn posperityXDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bitesToChangeIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn xBinAfterMutationDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn xAfterMutationDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fxFinalDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource geneticAlgorithmDataGridV2BindingSource7;
        private System.Windows.Forms.Label ErrorLabel;
        private System.Windows.Forms.Button TestingButton;
        private System.Windows.Forms.CheckBox isEliteCheckBox;
        private System.Windows.Forms.TextBox filePathTextBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}

