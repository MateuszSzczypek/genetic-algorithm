﻿using GeneticAlgorithm.Models;
using GeneticAlgorithm.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GeneticAlgorithm
{
    public partial class MainForm : Form
    {
        public static List<GeneticAlgorithmDataGridV2> population = new List<GeneticAlgorithmDataGridV2>();
        public List<GenerationsResultsDataGrid> generationsResults = new List<GenerationsResultsDataGrid>();

        public MainForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = population;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                int n = Convert.ToInt32(nTextBox.Text);
                double a = Convert.ToDouble(aTextBox.Text);
                double b = Convert.ToDouble(bTextBox.Text);
                double d = Convert.ToDouble(dTextBox.Text);
                int t = Convert.ToInt32(tTextBox.Text);
                double pk = Convert.ToDouble(pkTextBox.Text);
                double pm = Convert.ToDouble(pmTextBox.Text);
                bool isElite = Convert.ToBoolean(isEliteCheckBox.CheckState);

                GeneticAlgorithm(a, b, d, n, t, pk, pm, isElite);
                Visualisation(a, b, d, n, t, pk, pm, isElite);

                dataGridView1.DataSource = null;
                dataGridView1.Rows.Clear();
                dataGridView1.DataSource = population;
            }
            catch
            {
                ErrorLabel.Text = "Podałeś niepoprawne dane. Sprawdź kropki i przecinki przy liczbach zmiennoprzecinkowych";
            }
            
        }

        public void GeneticAlgorithm(double a, double b, double d, int n, int t, double pk, double pm, bool isElite)
        {
            Random random = new Random();
            population = new List<GeneticAlgorithmDataGridV2>();
            bool isFirstpopulation = true;
            GeneticAlgorithmDataGridV2 elite = new GeneticAlgorithmDataGridV2();
            generationsResults.Clear();

            //GeneticAlgorithmDataGrid v 3.0.0
            for (int generation = 0; generation < t; generation++)
            {
                if (isFirstpopulation)
                {
                    for (int i = 0; i < n; i++)
                    {
                        GeneticAlgorithmDataGridV2 tmp = new GeneticAlgorithmDataGridV2
                        {
                            Id = i + 1,
                            Xreal = Math.Round(RandomizerUtil.CreateRandom(a, b, ((int)d + 1) * 10000000), BitConverter.GetBytes(decimal.GetBits(System.Convert.ToDecimal(d))[3])[2])
                        };

                        tmp.Fx = Math.Round((tmp.Xreal % 1) * (Math.Cos(20 * Math.PI * tmp.Xreal) - Math.Sin(tmp.Xreal)), BitConverter.GetBytes(decimal.GetBits(System.Convert.ToDecimal(d))[3])[2]);
                        population.Add(tmp);
                    }
                    isFirstpopulation = false;
                }
                else
                {
                    //Czyszczenie population, aby kolejne pokolenie nie mialo smieciowych danych
                    List<GeneticAlgorithmDataGridV2> tmppopulation = new List<GeneticAlgorithmDataGridV2>(population);
                    population.Clear();
                    for (int i = 0; i < tmppopulation.Count(); i++)
                    {
                        GeneticAlgorithmDataGridV2 tmp = new GeneticAlgorithmDataGridV2
                        {
                            Id = tmppopulation[i].Id,
                            Xreal = tmppopulation[i].X_AfterMutation,
                            Fx = tmppopulation[i].FxFinal
                        };
                        population.Add(tmp);
                    }
                }

                //Jesli elita wlaczona to ja zapamietujemy
                if (isElite)
                {
                    elite = population.OrderByDescending(e => e.Fx).FirstOrDefault();
                }
                //
                
                double minFx = population.Min(takeMin => takeMin.Fx);

                foreach (var character in population)
                {
                    character.Gx = Math.Round((character.Fx - minFx + d), BitConverter.GetBytes(decimal.GetBits(System.Convert.ToDecimal(d))[3])[2]);
                }

                double gxSum = population.Sum(takeGxSum => takeGxSum.Gx);
                population.ElementAt(0).Px = population.ElementAt(0).Gx / gxSum;
                population.ElementAt(0).Qx = population.ElementAt(0).Px;
                population.ElementAt(0).R = random.NextDouble();

                for (int i = 1; i < n; i++)
                {
                    population.ElementAt(i).Px = population.ElementAt(i).Gx / gxSum;
                    population.ElementAt(i).Qx = population.ElementAt(i - 1).Qx + population.ElementAt(i).Px;
                    population.ElementAt(i).R = random.NextDouble();
                }

                //Dopasowanie po selekcji
                for (int i = 0; i < n; i++)
                {
                    for (int j = 0; j < n; j++)
                    {
                        if (population.ElementAt(i).R <= population.ElementAt(j).Qx)
                        {
                            population.ElementAt(i).X_RealAfterSelection = population.ElementAt(j).Xreal;
                            break;
                        }
                    }
                }

                //Krzyzujemy
                bool isCrossing = true;
                int tmpPc = -1;
                int firstElementInt = -1;

                for (int i = 0; i < n; i++)
                {
                    if (population.ElementAt(i).R <= pk)
                    {
                        population.ElementAt(i).X_ToCrossing = population.ElementAt(i).X_RealAfterSelection;
                        population.ElementAt(i).X_ToCrossingBin = ChapConverter.StringLengthDisplayer(Convert.ToString(ChapConverter.ConvertToInt(a, b, d, population.ElementAt(i).X_ToCrossing), 2), a, b, d);

                        if (isCrossing)
                        {
                            population.ElementAt(i).Pc = random.Next((int)ChapConverter.CountL(a, b, d));

                            tmpPc = population.ElementAt(i).Pc;
                            firstElementInt = i;

                            isCrossing = false;
                        }
                        else
                        {
                            population.ElementAt(i).Pc = tmpPc;

                            string tmpBinFirst1 = population.ElementAt(firstElementInt).X_ToCrossingBin.Substring(0, tmpPc);
                            string tmpBinFirst2 = population.ElementAt(i).X_ToCrossingBin.Substring(0, tmpPc);

                            string tmpBinLast1 = population.ElementAt(firstElementInt).X_ToCrossingBin.Substring(tmpPc, population.ElementAt(firstElementInt).X_ToCrossingBin.Length - tmpPc);
                            string tmpBinLast2 = population.ElementAt(i).X_ToCrossingBin.Substring(tmpPc, population.ElementAt(i).X_ToCrossingBin.Length - tmpPc);


                            population.ElementAt(firstElementInt).PosperityX = tmpBinFirst1 + tmpBinLast2;
                            population.ElementAt(i).PosperityX = tmpBinFirst2 + tmpBinLast1;

                            isCrossing = true;
                        }

                    }
                    else
                    {
                        population.ElementAt(i).PosperityX = ChapConverter.StringLengthDisplayer(Convert.ToString(ChapConverter.ConvertToInt(a, b, d, population.ElementAt(i).X_RealAfterSelection), 2), a, b, d);
                    }
                }

                if (population.Where(c => !string.IsNullOrWhiteSpace(c.X_ToCrossingBin)).Count() % 2 != 0)
                {
                    var unpairedId = population.Last(ajDi => !string.IsNullOrEmpty(ajDi.X_ToCrossingBin));
                    population.ElementAt(unpairedId.Id - 1).X_ToCrossing = population.ElementAt(unpairedId.Id - 1).X_RealAfterSelection;
                    population.ElementAt(unpairedId.Id - 1).X_ToCrossingBin = ChapConverter.StringLengthDisplayer(Convert.ToString(ChapConverter.ConvertToInt(a, b, d, population.ElementAt(unpairedId.Id - 1).X_ToCrossing), 2), a, b, d);

                    population.ElementAt(unpairedId.Id - 1).PosperityX = population.ElementAt(unpairedId.Id - 1).X_ToCrossingBin;
                }

                //Mutacja
                for (int i = 0; i < n; i++)
                {
                    StringBuilder sb = new StringBuilder(population.ElementAt(i).PosperityX);

                    for (int j = 0; j < population.ElementAt(i).PosperityX.Length; j++)
                    {
                        double r = random.NextDouble();

                        if ( r <= pm)
                        {
                            population.ElementAt(i).ListBitesToChangeID.Add(j);
                            population.ElementAt(i).BitesToChangeID += (j+1) + ", ";

                            if(sb[j] == '0')
                            {
                                sb[j] = '1';
                            }
                            else
                            {
                                sb[j] = '0';
                            }

                            population.ElementAt(i).XBinAfterMutation = sb.ToString();
                        }
                    }

                    if (string.IsNullOrEmpty(population.ElementAt(i).XBinAfterMutation)) population.ElementAt(i).XBinAfterMutation = population.ElementAt(i).PosperityX;

                    population.ElementAt(i).X_AfterMutation = Math.Round(ChapConverter.ConvertToXreal(a, b, d, Convert.ToInt32(population.ElementAt(i).XBinAfterMutation, 2)), BitConverter.GetBytes(decimal.GetBits(System.Convert.ToDecimal(d))[3])[2]);
                    population.ElementAt(i).FxFinal = Math.Round((population.ElementAt(i).X_AfterMutation % 1) * (Math.Cos(20 * Math.PI * population.ElementAt(i).X_AfterMutation) - Math.Sin(population.ElementAt(i).X_AfterMutation)), BitConverter.GetBytes(decimal.GetBits(System.Convert.ToDecimal(d))[3])[2]);
                }


                //Sprawdzamy czy elita jest w populacji po calym 1 przejsciu - po mutacji
                if (isElite)
                {
                    if (!population.Contains(elite))
                    {
                        int place = random.Next(n);
                        if (population.ElementAt(place).FxFinal < elite.Fx)
                        {
                            population[place] = elite;
                        }
                    }
                }
                //

                //Koniec generacji
                generationsResults.Add(
                    new GenerationsResultsDataGrid
                    {
                        ID = generation + 1,
                        Maximum = population.OrderByDescending(x => x.FxFinal).FirstOrDefault().FxFinal,
                        Average = population.Average(aa => aa.FxFinal),
                        Minimum = population.OrderBy(x => x.FxFinal).FirstOrDefault().FxFinal
                    });

            }
            //Koniec algorytmu

            
        }

        private void Visualisation(double a, double b, double d, int n, int t, double pk, double pm, bool isElite)
        {
            ResultForm rf = new ResultForm(a, b, d, n, t, pk, pm, isElite, generationsResults);
            rf.Show();

            //Create final results collection
            List<ResultDataGrid> tmpFinalResults = new List<ResultDataGrid>();
            foreach (var person in population)
            {
                ResultDataGrid tmp = new ResultDataGrid
                {
                    ID = person.Id,
                    Fx = person.FxFinal,
                    Xreal = person.X_AfterMutation,
                    Xbin = person.XBinAfterMutation
                };
                tmpFinalResults.Add(tmp);
            }

            var count = tmpFinalResults.GroupBy(x => x)
              .Where(g => g.Count() > 0)
              .Select(y => new { Element = y.Key, Counter = y.Count() })
              .ToList();

            count = count.OrderByDescending(x => x.Element.Fx).Select(x => x).ToList();
            int iter = 1;
            foreach (var item in count)
            {
                item.Element.ID = iter;
                iter++;
            }

            foreach (var item in count)
            {
                item.Element.Percentage = (Convert.ToDouble(item.Counter) / Convert.ToDouble(n)) * 100.0;
            }

            FinalResultForm frf = new FinalResultForm(count.Select(x => x.Element).ToList());
            frf.Show();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void TestingButton_Click(object sender, EventArgs e)
        {
            ErrorLabel.Text = "Trwają testy, proszę czekać...";

            double d = Convert.ToDouble(dTextBox.Text);
            int a = Convert.ToInt32(aTextBox.Text);
            int b = Convert.ToInt32(bTextBox.Text);
            bool isElite = isEliteCheckBox.Checked;

            int n = 30;
            int t = 50;
            double pk = 0.5;
            double pm = 0.0001;
            
            List<ParametersModel> testResults = new List<ParametersModel>();
            StringBuilder csvContent = new StringBuilder();
            csvContent.AppendLine("N,T,Pk,Pm,F(x)max,F(x)avg");
            
            for (int nn = 0; nn < 11 ; nn++)
            {
                for (int tt = 0; tt < 11; tt++)
                {
                    for (int pkk = 0; pkk < 9; pkk++)
                    {
                        for (int pmm = 0; pmm < 21; pmm++)
                        {
                            List<ParametersModel> repeatingParameters = new List<ParametersModel>();
                            for (int xx = 0; xx < 100; xx++)
                            {
                                GeneticAlgorithm(a, b, d, n, t, pk, pm, isElite);
                                repeatingParameters.Add(new ParametersModel
                                {
                                    N = n,
                                    T = t,
                                    Pk = pk,
                                    Pm = pm,
                                    Fxmax = population.OrderByDescending(aa => aa.FxFinal).FirstOrDefault().FxFinal,
                                    Fxavg = population.Select(r => r.FxFinal).ToList().Aggregate((x1, x2) => x1 + x2) / population.Count()
                                });
                            }

                            //Adding average results from multiplaying tests for GA with same n, t, pk, pm
                            testResults.Add(new ParametersModel
                            {
                                N = n,
                                T = t,
                                Pk = pk,
                                Pm = pm,
                                Fxmax = repeatingParameters.Select(r => r.Fxmax).ToList().Aggregate((x1, x2) => x1 + x2) / population.Count(),
                                Fxavg = repeatingParameters.Select(r => r.Fxavg).ToList().Aggregate((x1, x2) => x1 + x2) / population.Count()
                            });

                            pm = (pm != 0.0001) ? Math.Round(pm + 0.0005, 5) : 0.0005;
                        }
                        pm = 0.0001;
                        pk += Math.Round(0.05, 5);
                    }
                    pk = 0.5;
                    t += 10;
                }
                t = 50;
                n += 5;
            }

            //Choosing the best INPUT parameters n, t, pk, pm from tests.
            //Choosing -> Firstly Avg(Fx) Then from results if same Max(Fx) Then from results if same N * T -> MIN
            List<ParametersModel> bestParametersAvg = testResults.Where(x => x.Fxmax==testResults.Max(y => y.Fxavg)).ToList();
            List<ParametersModel> bestParametersAvgMax = bestParametersAvg.Where(x => x.Fxmax == bestParametersAvg.Max(y => y.Fxmax)).ToList();
            List<ParametersModel> bestParametersAvgMaxNxT = bestParametersAvgMax.OrderBy(aa => aa.N * aa.T).ToList();
            

            //Adding csv test results ordered from best to worst
            foreach (var testResult in bestParametersAvgMaxNxT)
            {
                csvContent.AppendLine(testResults.ToString());
            }

            foreach (var testResult in bestParametersAvgMax.Union(bestParametersAvgMaxNxT).ToList())
            {
                csvContent.AppendLine(testResults.ToString());
            }
            
            foreach (var testResult in bestParametersAvg.Union(bestParametersAvgMax).ToList())
            {
                csvContent.AppendLine(testResults.ToString());
            }

            //Save results to .csv file
            string testsResultsFileName = "testy.csv";
            File.AppendAllText(filePathTextBox.Text.ToString()+ testsResultsFileName, csvContent.ToString());

            //Info o zakonczeniu
            MessageBox.Show(String.Format("Zakończono wykonywanie testów.\nZapisano wyniki poszczególnych testów w pliku {0}{1}\nNajlepsze parametry to: {3}", 
                filePathTextBox.Text.ToString(), testsResultsFileName.ToString(), (bestParametersAvgMaxNxT == null) ? new ParametersModel().ToString() : bestParametersAvgMaxNxT.FirstOrDefault().ToString()));
            ErrorLabel.Text = "";

            tTextBox.Text = bestParametersAvgMaxNxT.FirstOrDefault().T.ToString();
            nTextBox.Text = bestParametersAvgMaxNxT.FirstOrDefault().N.ToString();
            pkTextBox.Text = bestParametersAvgMaxNxT.FirstOrDefault().Pk.ToString();
            pmTextBox.Text = bestParametersAvgMaxNxT.FirstOrDefault().Pm.ToString();
        }
        
    }
}