﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Utils
{
    public static class RandomizerUtil
    {
        private static Random random = new Random();
        public static double CreateRandom(double a, double b, int range)
        {
            double r = (a + (b-a) * (System.Convert.ToDouble(random.Next(0, range))))/ System.Convert.ToDouble(range);
            
            return r;
        }
        
    }
}
