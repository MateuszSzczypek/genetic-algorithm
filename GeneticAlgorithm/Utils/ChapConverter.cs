﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Utils
{
    public static class ChapConverter
    {
        public static int ConvertToInt(double a, double b, double d, double xreal)
        {
            double l = CountL(a, b, d);
            double result = (1 / (b - a)) * (xreal - a) * (Math.Pow(2, l) - 1);

            return System.Convert.ToInt32(result);
        }

        public static double ConvertToXreal(double a, double b, double d, int xint)
        {
            double l = CountL(a, b, d);

            return (((xint * (b - a)) / (Math.Pow(2, l) -1)) + a);
        }

        public static string StringLengthDisplayer(string bin, double a, double b, double d)
        {
            StringBuilder sb = new StringBuilder();
            int chromosomesNumber = (int)CountL(a, b, d);

            sb.Append(bin);
            if (bin.Length < chromosomesNumber + 1)
            {
                while (sb.Length < chromosomesNumber + 1)
                {
                    sb.Insert(0, "0");
                }
            }

            return sb.ToString();
        }
        

        public static double CountL(double a, double b, double d)
        {
            return (Math.Ceiling(Math.Log(((b - a) * ((1 / d) + 1)), 2)));
        }
    }
}
