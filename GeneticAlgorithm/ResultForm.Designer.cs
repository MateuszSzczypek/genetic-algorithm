﻿namespace GeneticAlgorithm
{
    partial class ResultForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title1 = new System.Windows.Forms.DataVisualization.Charting.Title();
            this.generationsResultsDataGrid = new System.Windows.Forms.DataGridView();
            this.InputInfoLabel = new System.Windows.Forms.Label();
            this.GenerationsResultsChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.minimumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.averageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.maximumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.generationsResultsDataGridBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.generationsResultsDataGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GenerationsResultsChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.generationsResultsDataGridBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // generationsResultsDataGrid
            // 
            this.generationsResultsDataGrid.AutoGenerateColumns = false;
            this.generationsResultsDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.generationsResultsDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.minimumDataGridViewTextBoxColumn,
            this.averageDataGridViewTextBoxColumn,
            this.maximumDataGridViewTextBoxColumn});
            this.generationsResultsDataGrid.DataSource = this.generationsResultsDataGridBindingSource;
            this.generationsResultsDataGrid.Location = new System.Drawing.Point(11, 95);
            this.generationsResultsDataGrid.Name = "generationsResultsDataGrid";
            this.generationsResultsDataGrid.Size = new System.Drawing.Size(462, 159);
            this.generationsResultsDataGrid.TabIndex = 0;
            // 
            // InputInfoLabel
            // 
            this.InputInfoLabel.AutoSize = true;
            this.InputInfoLabel.Location = new System.Drawing.Point(12, 9);
            this.InputInfoLabel.Name = "InputInfoLabel";
            this.InputInfoLabel.Size = new System.Drawing.Size(56, 13);
            this.InputInfoLabel.TabIndex = 19;
            this.InputInfoLabel.Text = "Informacje";
            // 
            // GenerationsResultsChart
            // 
            chartArea1.CursorY.LineColor = System.Drawing.Color.Lime;
            chartArea1.Name = "GenerationsResultsDefaultChartArea";
            this.GenerationsResultsChart.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.GenerationsResultsChart.Legends.Add(legend1);
            this.GenerationsResultsChart.Location = new System.Drawing.Point(15, 270);
            this.GenerationsResultsChart.Name = "GenerationsResultsChart";
            series1.ChartArea = "GenerationsResultsDefaultChartArea";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.Color = System.Drawing.Color.Red;
            series1.EmptyPointStyle.Color = System.Drawing.Color.White;
            series1.Legend = "Legend1";
            series1.MarkerBorderColor = System.Drawing.Color.White;
            series1.Name = "Minimum";
            series2.ChartArea = "GenerationsResultsDefaultChartArea";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            series2.Legend = "Legend1";
            series2.Name = "Average";
            series3.ChartArea = "GenerationsResultsDefaultChartArea";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series3.Color = System.Drawing.Color.Blue;
            series3.Legend = "Legend1";
            series3.Name = "Maximum";
            this.GenerationsResultsChart.Series.Add(series1);
            this.GenerationsResultsChart.Series.Add(series2);
            this.GenerationsResultsChart.Series.Add(series3);
            this.GenerationsResultsChart.Size = new System.Drawing.Size(458, 353);
            this.GenerationsResultsChart.TabIndex = 20;
            this.GenerationsResultsChart.Text = "Wykres dla każdej z generacji";
            title1.Name = "ChartTitle";
            title1.Text = "Wykres wyników z każdej generacji";
            this.GenerationsResultsChart.Titles.Add(title1);
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            // 
            // minimumDataGridViewTextBoxColumn
            // 
            this.minimumDataGridViewTextBoxColumn.DataPropertyName = "Minimum";
            this.minimumDataGridViewTextBoxColumn.HeaderText = "Minimum";
            this.minimumDataGridViewTextBoxColumn.Name = "minimumDataGridViewTextBoxColumn";
            // 
            // averageDataGridViewTextBoxColumn
            // 
            this.averageDataGridViewTextBoxColumn.DataPropertyName = "Average";
            this.averageDataGridViewTextBoxColumn.HeaderText = "Average";
            this.averageDataGridViewTextBoxColumn.Name = "averageDataGridViewTextBoxColumn";
            // 
            // maximumDataGridViewTextBoxColumn
            // 
            this.maximumDataGridViewTextBoxColumn.DataPropertyName = "Maximum";
            this.maximumDataGridViewTextBoxColumn.HeaderText = "Maximum";
            this.maximumDataGridViewTextBoxColumn.Name = "maximumDataGridViewTextBoxColumn";
            // 
            // generationsResultsDataGridBindingSource
            // 
            this.generationsResultsDataGridBindingSource.DataSource = typeof(GeneticAlgorithm.Models.GenerationsResultsDataGrid);
            // 
            // ResultForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(485, 635);
            this.Controls.Add(this.GenerationsResultsChart);
            this.Controls.Add(this.InputInfoLabel);
            this.Controls.Add(this.generationsResultsDataGrid);
            this.Name = "ResultForm";
            this.Text = "Wyniki generacji";
            this.Load += new System.EventHandler(this.ResultForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.generationsResultsDataGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GenerationsResultsChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.generationsResultsDataGridBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView generationsResultsDataGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn minimumDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn averageDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn maximumDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource generationsResultsDataGridBindingSource;
        private System.Windows.Forms.Label InputInfoLabel;
        private System.Windows.Forms.DataVisualization.Charting.Chart GenerationsResultsChart;
    }
}