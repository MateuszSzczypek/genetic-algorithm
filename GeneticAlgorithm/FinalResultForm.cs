﻿using GeneticAlgorithm.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GeneticAlgorithm
{
    public partial class FinalResultForm : Form
    {
        ICollection<ResultDataGrid> finalResults = new List<ResultDataGrid>();

        public FinalResultForm(ICollection<ResultDataGrid> finalResults)
        {
            InitializeComponent();

            this.finalResults = finalResults;
        }

        private void FinalResultForm_Load(object sender, EventArgs e)
        {
            bestPersonDataGrid.DataSource = finalResults;

            XrealLabel.Text = "Xreal = " + finalResults.OrderByDescending(a => a.Fx).FirstOrDefault().Xreal.ToString();
            XbinLabel.Text = "Xbin = " + finalResults.OrderByDescending(a => a.Fx).FirstOrDefault().Xbin.ToString();
            fxLabel.Text = "f(x) = " + finalResults.OrderByDescending(a => a.Fx).FirstOrDefault().Fx.ToString();
        }
    }
}
