﻿namespace GeneticAlgorithm
{
    partial class FinalResultForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bestPersonDataGrid = new System.Windows.Forms.DataGridView();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.xrealDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.xbinDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fxDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.percentageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.resultDataGridBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.bestPersonGroupBox = new System.Windows.Forms.GroupBox();
            this.fxLabel = new System.Windows.Forms.Label();
            this.XbinLabel = new System.Windows.Forms.Label();
            this.XrealLabel = new System.Windows.Forms.Label();
            this.resultDataGridBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.resultDataGridBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.bestPersonDataGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultDataGridBindingSource2)).BeginInit();
            this.bestPersonGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.resultDataGridBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultDataGridBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // bestPersonDataGrid
            // 
            this.bestPersonDataGrid.AutoGenerateColumns = false;
            this.bestPersonDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.bestPersonDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.xrealDataGridViewTextBoxColumn,
            this.xbinDataGridViewTextBoxColumn,
            this.fxDataGridViewTextBoxColumn,
            this.percentageDataGridViewTextBoxColumn});
            this.bestPersonDataGrid.DataSource = this.resultDataGridBindingSource2;
            this.bestPersonDataGrid.Location = new System.Drawing.Point(12, 118);
            this.bestPersonDataGrid.Name = "bestPersonDataGrid";
            this.bestPersonDataGrid.Size = new System.Drawing.Size(556, 449);
            this.bestPersonDataGrid.TabIndex = 0;
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            // 
            // xrealDataGridViewTextBoxColumn
            // 
            this.xrealDataGridViewTextBoxColumn.DataPropertyName = "Xreal";
            this.xrealDataGridViewTextBoxColumn.HeaderText = "Xreal";
            this.xrealDataGridViewTextBoxColumn.Name = "xrealDataGridViewTextBoxColumn";
            // 
            // xbinDataGridViewTextBoxColumn
            // 
            this.xbinDataGridViewTextBoxColumn.DataPropertyName = "Xbin";
            this.xbinDataGridViewTextBoxColumn.HeaderText = "Xbin";
            this.xbinDataGridViewTextBoxColumn.Name = "xbinDataGridViewTextBoxColumn";
            // 
            // fxDataGridViewTextBoxColumn
            // 
            this.fxDataGridViewTextBoxColumn.DataPropertyName = "Fx";
            this.fxDataGridViewTextBoxColumn.HeaderText = "Fx";
            this.fxDataGridViewTextBoxColumn.Name = "fxDataGridViewTextBoxColumn";
            // 
            // percentageDataGridViewTextBoxColumn
            // 
            this.percentageDataGridViewTextBoxColumn.DataPropertyName = "Percentage";
            this.percentageDataGridViewTextBoxColumn.HeaderText = "Percentage";
            this.percentageDataGridViewTextBoxColumn.Name = "percentageDataGridViewTextBoxColumn";
            // 
            // resultDataGridBindingSource2
            // 
            this.resultDataGridBindingSource2.DataSource = typeof(GeneticAlgorithm.Models.ResultDataGrid);
            // 
            // bestPersonGroupBox
            // 
            this.bestPersonGroupBox.Controls.Add(this.fxLabel);
            this.bestPersonGroupBox.Controls.Add(this.XbinLabel);
            this.bestPersonGroupBox.Controls.Add(this.XrealLabel);
            this.bestPersonGroupBox.Location = new System.Drawing.Point(12, 12);
            this.bestPersonGroupBox.Name = "bestPersonGroupBox";
            this.bestPersonGroupBox.Size = new System.Drawing.Size(556, 100);
            this.bestPersonGroupBox.TabIndex = 1;
            this.bestPersonGroupBox.TabStop = false;
            this.bestPersonGroupBox.Text = "Najlepszy osobnik";
            // 
            // fxLabel
            // 
            this.fxLabel.AutoSize = true;
            this.fxLabel.Location = new System.Drawing.Point(18, 75);
            this.fxLabel.Name = "fxLabel";
            this.fxLabel.Size = new System.Drawing.Size(21, 13);
            this.fxLabel.TabIndex = 2;
            this.fxLabel.Text = "f(x)";
            // 
            // XbinLabel
            // 
            this.XbinLabel.AutoSize = true;
            this.XbinLabel.Location = new System.Drawing.Point(18, 52);
            this.XbinLabel.Name = "XbinLabel";
            this.XbinLabel.Size = new System.Drawing.Size(28, 13);
            this.XbinLabel.TabIndex = 1;
            this.XbinLabel.Text = "Xbin";
            // 
            // XrealLabel
            // 
            this.XrealLabel.AutoSize = true;
            this.XrealLabel.Location = new System.Drawing.Point(18, 26);
            this.XrealLabel.Name = "XrealLabel";
            this.XrealLabel.Size = new System.Drawing.Size(31, 13);
            this.XrealLabel.TabIndex = 0;
            this.XrealLabel.Text = "Xreal";
            // 
            // resultDataGridBindingSource1
            // 
            this.resultDataGridBindingSource1.DataSource = typeof(GeneticAlgorithm.Models.ResultDataGrid);
            // 
            // resultDataGridBindingSource
            // 
            this.resultDataGridBindingSource.DataSource = typeof(GeneticAlgorithm.Models.ResultDataGrid);
            // 
            // FinalResultForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(580, 579);
            this.Controls.Add(this.bestPersonGroupBox);
            this.Controls.Add(this.bestPersonDataGrid);
            this.Name = "FinalResultForm";
            this.Text = "Wyniki końcowe";
            this.Load += new System.EventHandler(this.FinalResultForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bestPersonDataGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultDataGridBindingSource2)).EndInit();
            this.bestPersonGroupBox.ResumeLayout(false);
            this.bestPersonGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.resultDataGridBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultDataGridBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView bestPersonDataGrid;
        private System.Windows.Forms.BindingSource resultDataGridBindingSource;
        private System.Windows.Forms.GroupBox bestPersonGroupBox;
        private System.Windows.Forms.Label fxLabel;
        private System.Windows.Forms.Label XbinLabel;
        private System.Windows.Forms.Label XrealLabel;
        private System.Windows.Forms.BindingSource resultDataGridBindingSource1;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn xrealDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn xbinDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fxDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn percentageDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource resultDataGridBindingSource2;
    }
}