﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Models
{
    public class GeneticAlgorithmDataGridV2
    {
        public int Id { get; set; }
        public double Xreal { get; set; }
        public double Fx { get; set; }
        public double Gx { get; set; }
        public double Px { get; set; }
        public double Qx { get; set; }
        public double R { get; set; }
        public double X_RealAfterSelection { get; set; }
        public double X_ToCrossing { get; set; }
        public string X_ToCrossingBin { get; set; }
        public int Pc { get; set; }
        public string PosperityX { get; set; }
        public string BitesToChangeID { get; set; }
        public List<int> ListBitesToChangeID { get; set; } = new List<int>();
        public string XBinAfterMutation { get; set; }
        public double X_AfterMutation { get; set; }
        public double FxFinal { get; set; }

    }
}
