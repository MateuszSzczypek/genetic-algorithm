﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Models
{
    public class GenerationsResultsDataGrid
    {
        public int ID { get; set; }
        public double Minimum { get; set; }
        public double Average { get; set; }
        public double Maximum { get; set; }

    }
}
