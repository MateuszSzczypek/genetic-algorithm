﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Models
{
    public class ResultDataGrid
    {
        public int ID { get; set; }
        public double Xreal { get; set; }
        public string Xbin { get; set; }
        public double Fx { get; set; }
        public double Percentage { get; set; }

        public override bool Equals(object obj)
        {
            var item = obj as ResultDataGrid;

            if(item == null)
            {
                return false;
            }
            
            return this.Xreal.Equals(item.Xreal);
        }

        public override int GetHashCode()
        {
            return this.Xreal.GetHashCode();
        }
    }
}
