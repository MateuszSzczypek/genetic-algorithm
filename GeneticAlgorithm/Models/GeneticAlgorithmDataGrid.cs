﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Models
{
    public class GeneticAlgorithmDataGrid
    {
        public int Id { get; set; }
        public double Xreal { get; set; }
        public int Xint { get; set; }
        public string Xbin { get; set; }
        public int XintCheck { get; set; }
        public double XrealCheck { get; set; }
        public double RatingFunction { get; set; }
    }
}
