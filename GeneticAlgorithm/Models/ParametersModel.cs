﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Models
{
    public class ParametersModel
    {
        public int N { get; set; }
        public int T { get; set; }
        public double Pk { get; set; }
        public double Pm { get; set; }

        public double Fxmax { get; set; }
        public double Fxavg { get; set; }


        public override string ToString()
        {
            return (this.N.ToString() + " " + this.T.ToString() + " " + this.Pk.ToString() + " " + this.Pm.ToString() + " " + this.Fxmax.ToString() + " " + this.Fxavg.ToString());
        }
    }
}
