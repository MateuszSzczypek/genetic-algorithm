﻿using GeneticAlgorithm.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GeneticAlgorithm
{
    public partial class ResultForm : Form
    {
        private ICollection<GenerationsResultsDataGrid> generationsResults;

        public ResultForm(double a, double b, double d, int n, int t, double pk, double pm, bool isElite, ICollection<GenerationsResultsDataGrid> generationsResults)
        {
            InitializeComponent();

            this.generationsResults = generationsResults;
            InputInfoLabel.Text = "Input data:\nX0 e <a,b>, a = " + a.ToString() + ", b = " + b.ToString() + ", dokładność = " + d.ToString() 
                + "\nLiczba osobników w populacji = " + n.ToString() + ", liczba generacji = " + t.ToString() 
                + "\nPrawdopodobieństwo krzyżowania = " + pk.ToString() + ", prawdopodobieństwo mutacji = " + pm.ToString()
                + "\n" + (isElite ? "Elita jest włączona" : "Elita nie jest włączona");
        }

        private void ResultForm_Load(object sender, EventArgs e)
        {
            generationsResultsDataGrid.DataSource = generationsResults;

            foreach (var generationResult in generationsResults)
            {
                this.GenerationsResultsChart.Series["Minimum"].Points.AddXY(generationResult.ID, generationResult.Minimum);
                this.GenerationsResultsChart.Series["Average"].Points.AddXY(generationResult.ID, generationResult.Average);
                this.GenerationsResultsChart.Series["Maximum"].Points.AddXY(generationResult.ID, generationResult.Maximum);
            }
            GenerationsResultsChart.ChartAreas["GenerationsResultsDefaultChartArea"].AxisX.Title = "T";
            GenerationsResultsChart.ChartAreas["GenerationsResultsDefaultChartArea"].AxisY.Title = "f(x)";
        }

    }
}
